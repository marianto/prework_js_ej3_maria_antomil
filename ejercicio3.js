

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var numeroDni = parseInt(prompt("Introduce el numero de su DNI", 0));
var letraDni = prompt("Introduce la letra de su DNI (en mayusculas)");

letraDni = letraDni.toUpperCase();

if(numeroDni < 0 || numeroDni > 99999999) {
    alert("DNI incorrecto");

}
else {
  var letra = letras[numeroDni % 23];
  if(letra != letraDni) {
    alert("La letra DNI no es correcta");
  }
  else {
    alert("DNI es válido");
  }
}